node {

    def mvn="/opt/apache-maven/bin/mvn"
	def oc = '/opt/openshift-v3.11.0/oc'
    def environment = "prod"

    def cpnt_ws="mdcf-service-support-notification-sms-v1"

    def tag="mdcf"

    stage('Preparation') { // for display purposes
        checkout scm
    }

    stage('Build') {
        sh "${mvn} clean compile install -U"
    }

    stage('Docker Build') {
        sh "mv ./devops/docker/Dockerfile ./Dockerfile"
        sh "docker build -t ${tag}/${cpnt_ws} --build-arg environment=${environment} --no-cache ."
    }

    stage('Docker Push OC') {
        withCredentials([
                usernamePassword(credentialsId: 'PROD_OPENSHIFT_USER_PASS', passwordVariable: 'OC_PASS', usernameVariable: 'OC_USER')
        ]) {
            sh "${oc} login console.barriotaxi.com:8443 -u ${OC_USER} -p ${OC_PASS} -n ${tag} --insecure-skip-tls-verify"

            sh "docker login -u `${oc} whoami` -p `${oc} whoami -t` registry.apps.barriotaxi.com"

            sh "docker tag ${tag}/${cpnt_ws} registry.apps.barriotaxi.com/${tag}/${cpnt_ws}"
            sh "docker push registry.apps.barriotaxi.com/${tag}/${cpnt_ws}"
        }
    }

    stage('Create App') {

        sh "${oc} delete -f ./devops/deploy/application.yaml -n ${tag} || true"
        sh "${oc} apply -f ./devops/deploy/application.yaml -n ${tag}"

    }

}