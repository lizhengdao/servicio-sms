import serial
import os
import time

#port = serial.Serial(SERIAL_PORT, baudrate = 9600, timeout = 5)
SERIAL_PORT = "/dev/ttyS0"
def connect():
    try:
        connection = serial.Serial(SERIAL_PORT, baudrate = 9600, timeout = None,xonxoff=0, rtscts=0)
            
        return connection
    except Exception as e:
        print("no se pudo conectar")
            
def setup(port):
    print("Iniciando SIM900...")
    port.write('AT'+'\r')
    time.sleep(1)
    print(".")
    port.write('ATE0'+'\r')
    time.sleep(1)
    print(".")
    port.write('AT+CMGF=1'+'\r')
    time.sleep(1)
    print(".")
    port.write('AT+CNMI=2,1,0,0,0'+'\r')
    time.sleep(1)
    print("Configuracion Sim900 terminada...")
    

def send_message(port, phoneNumber, message):
    port.write('AT+CMGS="'+phoneNumber+'"'+'\r')
    time.sleep(0.5)
    port.write(message+'\r')
    time.sleep(0.5)
    port.write("\x1a")
    time.sleep(4)
    print("mensaje Enviado")
