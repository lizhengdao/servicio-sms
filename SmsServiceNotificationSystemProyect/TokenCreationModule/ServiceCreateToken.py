import paho.mqtt.client as mqtt # importando al cliente
import time
import datetime

from utility.database import DataBasev2 as DB
from utility.encrypt import EncodeDecodeMessage as EDM


broker_address="broker.mqtt-dashboard.com"
topics = ["in_token_security"]

table_name = 'Tokens'


path_data_base = '/home/pi/Documents/SmsServiceNotificationSystemProyect/DataBase/SERVICESMS.db'
def on_connect(client, userdata, flags, rc):
    print("Connected!", str(rc))
    time.sleep(1)
    client.subscribe(topics[0])
    
def on_message(client, userdata, message):
    if '//**//' in message.payload.decode("utf-8"):
        messageSplit = message.payload.split('//**//')
        
        tokenSha256 = messageSplit[0]
        salt = messageSplit[1]
        userCypher = messageSplit[2]
        
        userDecode = EDM.decrypt_with_AES(userCypher,tokenSha256,salt)
        userAux = userDecode.decode("utf-8")
        
        registrationDate = datetime.datetime.now()
        date_time = registrationDate.strftime("%m/%d/%Y %H:%M:%S")
        
        con = DB.sql_connection(path_data_base)
        columns = '(id integer PRIMARY KEY AUTOINCREMENT, token_entrance TEXT NOT NULL, salt TEXT NOT NULL, user TEXT NOT NULL, date_registered TEXT NOT NULL)'
        DB.sql_table(con,table_name,columns)
        
        columnsInsert = ('token_entrance', 'salt', 'user', 'date_registered')
        data = (tokenSha256,salt,str(userAux),date_time)
        DB.sql_insert(con, table_name, columnsInsert, data)
        
        print("Inserted Data...")
        con.close();
        

client = mqtt.Client("client_service_creation_token")
client.connect(broker_address)
time.sleep(1)
client.on_connect = on_connect
client.on_message = on_message
time.sleep(1)

client.loop_forever()



