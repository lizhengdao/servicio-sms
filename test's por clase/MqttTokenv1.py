import paho.mqtt.client as paho # importando al cliente
import time
import datetime
import sqlite3

import base64
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet

token = ''
broker_address="broker.hivemq.com" #Servidor mosquito


# CONECCION CON LA BASE DE DATOS
dbase = sqlite3.connect('MQTTSIM900.db')#open database file
cursor = dbase.cursor()



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # #Encryptacion de Mensages por password# # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def get_key(token):

    digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
    digest.update(token)
    return base64.urlsafe_b64encode(digest.finalize())

def encrypt(token, message):
    f = Fernet(get_key(token))
    return f.encrypt(bytes(message))

def decrypt(token, message):
    f = Fernet(get_key(token))
    return f.decrypt(bytes(message))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # #Gestion de la Base de datos# # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def create_table():
    dbase.execute(''' CREATE TABLE IF NOT EXISTS SMS_SERVICE2(
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    phoneNumber TEXT NOT NULL,
    message TEXT NOT NULL,
    registrationDate TEXT NOT NULL,
    token TEXT NOT NULL) ''')

    print 'Table created'
    
def read_Data():
    # from math import *
    data = cursor.execute(''' SELECT * FROM SMS_SERVICE2 ORDER BY phoneNumber''')
    for record in data:
        print 'ID : '+str(record[0])
        print 'PHONE NUMBER : '+str(record[1])
        print 'MESSAGE : '+str(record[2])
        print 'REGISTRATION DATE : '+str(record[3])
        print 'TOKEN : '+str(record[4])+'\n'
        
def insert_Data(phoneNumber, message, registrationDate,token):
    data = "INSERT INTO SMS_SERVICE(phoneNumber,message,registrationDate, token) VALUES(?,?,?,?)"
        
    dbase.execute(data, (phoneNumber,message,registrationDate,token))
    
    dbase.commit()
    print 'Record inserted'
    
def access_token(token):
    data = cursor.execute(''' SELECT token FROM TOKEN''')
    for record in data:
        aux = str(record[0])
        if(aux == token):
            return token.encode();
    return None  
    
    
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # #Gestion de Libreria MQTT # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

#Se Suscribe a los topicos asignados
def on_connect(client, userdata, flags, rc):
    print("Connected!", str(rc))
    time.sleep(1)
    client.subscribe("in_message")
    
#Se Muestra todos los mensajes recibidos desde MQTT
def on_message(client, userdata, message):
    #print(datetime.datetime.now())
    messageDesc = message.payload.split('///*///') 
    time.sleep(1)
    #print("received message =",str(message.payload.decode("utf-8")))
    print("mensaje1 = ",messageDesc[0])
    print("mensaje2 = ",messageDesc[1])
    print("token = ",messageDesc[2])
    
    if(access_token(messageDesc[2])):
        token = access_token(messageDesc[2])
        print(token)
        phoneNumber = decrypt(token, messageDesc[0])
        messageSMS = decrypt(token, messageDesc[1])
        
        print(phoneNumber)
        print(messageSMS)
        
    
    

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #     
#conexcion con el servidor MQTT
print("creating new Client")
client = paho.Client("SMS_Service")#Se crea el Cliente
client.connect(broker_address)
time.sleep(1)
#configuracion incial
client.on_connect = on_connect
client.on_message = on_message
time.sleep(1)


create_table()
#insert_Data("1123456789","hellow bb","mananaxd","WDrevvK8ZrPn8gmiNFjcOp2xovBr40TCwJlZOyI94IY=")
#read_Data()


client.loop_forever()


